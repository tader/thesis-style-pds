% tudelft-ewi-pds-thesis.cls
% Copyright 2010, Thomas A. de Ruiter <thomas@de-ruiter.cx>
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{tudelft-ewi-pds-thesis}[2010/11/24 Thesis class PDS Group, Delft University of Technology]

\newif\if@review\@reviewfalse
\DeclareOption{review}{\@reviewtrue\PassOptionsToClass{draft}{report}}

\newif\if@tuhuisstijl\@tuhuisstijlfalse
\DeclareOption{tuhuisstijl}{\@tuhuisstijltrue}

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{report}}

\ExecuteOptions{11pt,twoside,a4paper,openright}
\ProcessOptions

\LoadClass[11pt,twoside,a4paper,openright]{report}

\RequirePackage[british]{babel}
\RequirePackage{graphicx}
\RequirePackage[unicode=true,bookmarks=true,bookmarksnumbered=true,
 bookmarksopen=false,breaklinks=false,pdfborder={0 0 0},
 backref=false,colorlinks=false,pdfcreator={LaTeX}]{hyperref}
\RequirePackage{url}
\RequirePackage{setspace}
\RequirePackage{lineno}

\if@tuhuisstijl
	\RequirePackage{color}
	\RequirePackage{xcolor}
	\RequirePackage{rotating}
	\RequirePackage{wallpaper}
	\RequirePackage{titlesec}
	\setlength{\wpXoffset}{-1.63cm}
	\setlength{\wpYoffset}{-.26cm}
	\renewcommand{\familydefault}{\sfdefault}
	\titleformat{\chapter}[display]
		{\normalfont\bfseries\Huge}
		{\hfill\thechapter}{20pt}{\normalfont\rmfamily\Huge\bfseries\color{cyan}\hfill}
	\titleformat{\section}
		{\normalfont\rmfamily\Large\bfseries\color{cyan}}
		{\thesection}{1em}{}
	\titleformat{\subsection}
		{\normalfont\rmfamily\large\bfseries}
		{\thesubsection}{1em}{}
\else
	\RequirePackage{times}
\fi

\if@review
	\linenumbers\doublespacing
\fi

\hoffset=1.63cm
\oddsidemargin=0in
\evensidemargin=0in
\textwidth=5in
\parindent=1em

\setcounter{page}{1}
\pagenumbering{roman}


% fields that user can set (in addition to title, author, etc)
\makeatletter
	% Thesis Subtitle
	\newcommand{\subtitle}[1]{\def \@subtitle {#1}}
	\subtitle{}

	% Author email address
	\newcommand{\email}[1]{\def \@email {#1}}
	\email{}

	% Kind of report (probably MSc Thesis)
	\newcommand{\reporttype}[1]{\def \@reporttype {#1}}
	\reporttype{Master's Thesis in Computer Science}

	% Kind of presentation (probably MSc Presentation)
	\newcommand{\presentationtype}[1]{\def \@presentationtype {#1}}
	\presentationtype{MSc Presentation}

	% Date of presentation
	\newcommand{\presentationdate}[1]{\def \@presentationdate {#1}}
	\presentationdate{}

	% Group
	\newcommand{\group}[1]{\def \@group {#1}}
	\group{Parallel and Distributed Systems Group\\
		Faculty of Electrical Engineering, Mathematics,
		and Computer Science\\
		Delft University of Technology}

	% Kind of Committee
	\newcommand{\graduationcommitteename}[1]{\def \@graduationcommitteename {#1}}
	\graduationcommitteename{Graduation Committee}	

	% Committee Members
	\newcommand{\graduationcommittee}[1]{\def \@graduationcommittee {#1}}
	\graduationcommittee{}
\makeatother


% Title Pages
\makeatletter
\def \maketitle {%
	\pagestyle{empty} % disable page numbering
	\hypersetup{pdftitle={\@title},pdfauthor={\@author},pdfsubject={\@reporttype}}
	% Cover Page
	\begin{titlepage}
		\if@tuhuisstijl
			\CenterWallPaper{1}{TU_border_A4_P_front}
			\begin{picture}(0,0)(0,0)
				\put(-53,-50){ % Lekker vies "marge" maken ;)
					\colorbox{black}{
						\parbox[l][150pt][t]{0.5\paperwidth}{
							...
						}
					}
				}
				\put(-43,-50){
					\colorbox{black}{
						\parbox[l][150pt][t]{0.5\paperwidth}{
							\vspace{0.5cm}
							\textcolor{white}{\textrm{%
								\textbf{\Huge\@title}}} \\

							\vfill
							\textcolor{cyan}{\textbf{\textrm{%
								\Large\@author}}}
							\vspace{0.5cm}
						}
					}
				}
				\put(-133,-206){
					\colorbox{cyan}{
						\parbox[l][150pt][b]{66.6pt}{
							\hfill
							\begin{sideways}
							\textcolor{white}{% Some text possible
							}
							\end{sideways}
						}
					}
				}
			\end{picture}
		\else
			\null\vspace{4cm}
			\begin{center}
				{\LARGE \@title} \\
				\ifx \@subtitle \@empty
					\relax
				\else
					\vspace{0.5em}
					{\Large \@subtitle} \\
				\fi
				
				\ifx \@author \@empty
					\relax
				\else
					\vspace{1.5cm}
					\@author \\
				\fi

				\vfill
				\if@tuhuisstijl
					\relax
				\else
					\includegraphics[width={0.5\textwidth}]{TUD_logo_color}
				\fi

			\end{center}
			\vspace{2.0cm}
		\fi
	\end{titlepage}
		\if@tuhuisstijl
			\clearpage
			\ClearWallPaper
		\fi
	\cleardoublepage % start next page on the right

	% Title Page
	\begin{titlepage}
		\null\vspace{4cm}
		\begin{center}
			{\LARGE \@title} \\
			\ifx \@subtitle \@empty
				\relax
			\else
				\vspace{0.5em}
				{\Large \@subtitle} \\
			\fi

			\vspace{3cm}

			{\large \@reporttype} \\

			\vspace{1.5cm}

			{\normalsize \@group} \\

			\vspace{2.0cm}

			{\normalsize \@author} \\

			\vspace{1.0cm}

			\@date
		\end{center}
		\vfill
	\end{titlepage}

	% Title Page Backside
	\noindent \textbf{Author}\\
	\begin{tabular}{l}
		\@author
		\ifx \@email \@empty
			\\
		\else
			{ }<\href{mailto:\@email}{\@email}>\\
		\fi
	\end{tabular}

	\noindent \textbf{Title}\\
	\begin{tabular}{l}
		\@title\\
	\end{tabular}

	\ifx \@presentationdate \@empty
		\relax
	\else
		\noindent \textbf{\@presentationtype}\\
		\begin{tabular}{l}
			\@presentationdate\\
			\\
		\end{tabular}
	\fi

	\ifx \@graduationcommittee \@empty
		\relax
	\else
		\vspace{1.1cm}

		\noindent \textbf{\@graduationcommitteename}\\
		\begin{tabular}{ll}
			\@graduationcommittee
		\end{tabular}
	\fi
	
	\cleardoublepage % start next page on the right
	
	\pagestyle{plain} % enable page numbering
}
\makeatother


% redefining titlepage environment
% so it will not reset the page number
\makeatletter
\if@compatibility
\renewenvironment{titlepage}
    {%
      \if@twocolumn
        \@restonecoltrue\onecolumn
      \else
        \@restonecolfalse\newpage
      \fi
      \thispagestyle{empty}%
      %\setcounter{page}\z@
    }%
    {\if@restonecol\twocolumn \else \newpage \fi
    }
\else
\renewenvironment{titlepage}
    {%
      \if@twocolumn
        \@restonecoltrue\onecolumn
      \else
        \@restonecolfalse\newpage
      \fi
      \thispagestyle{empty}%
      %\setcounter{page}\@ne
    }%
    {\if@restonecol\twocolumn \else \newpage \fi
     \if@twoside\else
        %\setcounter{page}\@ne
     \fi
    }
\fi
\makeatother

\newenvironment{mainmatter}{\cleardoublepage\pagenumbering{arabic}\setcounter{page}{1}}{}

